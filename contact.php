<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>CONTACT</title>

    <meta property="og:image" content="logo">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <link rel="shortcut icon" href="logo-shortcut">


    <?php
    include_once '_relativePath.php';
    //ALL STYLE SHEET FILES
    include_once $relPath.'assets_frontEnd/_stylesheet.php';
    ?>

</head>
<body>
<div class="se-pre-con"></div>

<?php
$curPage=3;

include_once $relPath.'_header.php'; ?>

<div class="container">
    <div class="">
        <table class="table table-responsive table-sm table-bordered">
            <thead><tr><th scope="col">Item #</th><th scope="col">Product or Service</th><th scope="col">Price (ea.)</th><th scope="col">Retail Price (Case)</th><th scope="col">Case Discount</th><th scope="col">Wholesale Price</th><th scope="col">Wholesale Discount</th></tr></thead>
            <tbody>
            <tr><th scope="row">100050</th><td>Advance Pet Oral Care Toothbrush and Toothpaste</td><td>$9.55 </td><td>$108.87</td><td>$5.73</td><td>$103.14</td><td>$11.46</td></tr>
            <tr><th scope="row">100043</th><td>Basic Teeth Cleaning and Exam</td><td>$100.00 </td><td>$1,140.00</td><td>$60.00</td><td>$1,080.00</td><td>$120.00</td></tr>
           <tr class="table-success"><th scope="row">100013</th><td>Calm Cat Anxiety Relief Spray</td><td>$9.49 </td><td>$108.19</td><td>$5.69</td><td>$102.49</td><td>$11.39</td></tr>
           <tr class="table-active"><th scope="row">100041</th><td>Cat Hairball Remedy Gel</td><td>$6.00 </td><td>$68.40</td><td>$3.60</td><td>$64.80</td><td>$7.20</td></tr>
           <tr class="table-info"><th scope="row">100051</th><td>Cat Vaccination Package</td><td>$55.00 </td><td>$627.00</td><td>$33.00</td><td>$594.00</td><td>$66.00</td></tr>
           <tr class="table-warning"><th scope="row">100046</th><td>Dog Vaccination Package</td><td>$65.00 </td><td>$741.00</td><td>$39.00</td><td>$702.00</td><td>$78.00</td></tr>
           <tr class="table-danger"><th scope="row">100044</th><td>Healthy Coat Dog Supplement</td><td>$6.44 </td><td>$73.42</td><td>$3.86</td><td>$69.55</td><td>$7.73</td></tr>
           <tr class="table-primary text-white"><th scope="row">100030</th><td>Healthy Coat Dog Supplement</td><td>$9.56 </td><td>$108.98</td><td>$5.74</td><td>$103.25</td><td>$11.47</td></tr>
            <tr><th scope="row">100045</th><td>Here Kitty Kitty Organic Catnip</td><td>$7.75 </td><td>$88.35</td><td>$4.65</td><td>$83.70</td><td>$9.30</td></tr>
             </tbody>
        </table>
    </div><!-- table-responsive -->
</div><!-- table-container -->



<!--- THE CONTENT HERE ----->

<div class='container'>
<h2> <span class='fa fa-vcard'> </span> Contact.. </h2>
</div>



<?php  include_once $relPath.'_footer.php';

// ALL JAVASCRIPT FILES
include_once $relPath.'assets_frontEnd/_javascript.php';
?>

</body>
</html>