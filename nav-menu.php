<div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="/assets_FrontEnd/images/faces/6.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Ricardo Cossa</p>
                  <div>
                    <small class="designation text-muted">Gestor</small>
                    
                    <span class="status-indicator online"></span>
                      <p class="designation text-muted">Farmacia 24H</p>
                  </div>
                </div>
              </div>
              <button class="btn btn-success btn-block">Nova Pedido
                <i class="mdi mdi-plus"></i>
              </button>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/home">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Inicio</span>
            </a>
          </li>
<!--
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Gerenciar</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="javascript:;">Entidades</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="javascript:;">Medicamentos</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:;">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Farmacias</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:;">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Pedidos</span>
            </a>
          </li>
            
              <li class="nav-item">
            <a class="nav-link" href="javascript:;">
              <i class="menu-icon mdi mdi-chart-areaspline"></i>
              <span class="menu-title">Registos</span>
            </a>
          </li>
-->
          
        </ul>
      </nav>