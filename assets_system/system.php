<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>System</title>
    <meta property="og:image" content="logo">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <link rel="shortcut icon" href="logo-shortcut">


    <?php
    include_once '_relativePath.php';
    //ALL STYLE SHEET FILES
    include_once $relPath.'assets_frontEnd/_stylesheet.php';
    ?>

</head>
<body>
<div class="se-pre-con"></div>

<?php
$curPage=4;
include_once '_relativePath.php';
include_once $relPath.'_dir.php';
include_once $relPath.'_header.php'; ?>


<!--- THE CONTENT HERE ----->
<h3>CAROUSEL</h3>
<div class="container">

    <div class="carousel slide" data-ride="carousel" id="featured">

        <ol class="carousel-indicators">
            <li data-target="#featured" data-slide-to="0" class="active"></li>
            <li data-target="#featured" data-slide-to="1"></li>
            <li data-target="#featured" data-slide-to="2"></li>
            <li data-target="#featured" data-slide-to="3"></li>
            <li data-target="#featured" data-slide-to="4"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active"><img src="/assets_frontEnd/_img/vbuilder-slogan.png" alt="Lifestyle Photo"></div>
            <div class="carousel-item"><img src="/assets_frontEnd/_img/vbuilder-slogan.png" alt="Mission"></div>
            <div class="carousel-item"><img src="/assets_frontEnd/_img/vbuilder-slogan.png" alt="Vaccinations"></div>
            <div class="carousel-item"><img src="/assets_frontEnd/_img/vbuilder-slogan.png" alt="Fish"></div>
            <div class="carousel-item"><img src="/assets_frontEnd/_img/vbuilder-slogan.png" alt="Exotic Animals"></div>
        </div><!-- carousel-inner -->

        <a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#featured" role="button" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div><!-- featured carousel -->
</div><!-- content container -->


<div class='container'>
    <h2><span class='glyphicon glyphicon-wrench'> </span>  Demo.. </h2>

    <?php  include_once $relPath.'assets_system_includes/_create/addDemo.php';
    ?>

</div>



<?php  include_once $relPath.'_footer.php';

// ALL JAVASCRIPT FILES
include_once $relPath.'assets_frontEnd/_javascript.php';
?>

</body>
</html>