<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>DEMO</title>
    <meta property="og:image" content="logo">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <link rel="shortcut icon" href="logo-shortcut">


    <?php
    include_once '_relativePath.php';
    //ALL STYLE SHEET FILES
    include_once $relPath.'assets_frontEnd/_stylesheet.php';
    ?>

</head>
<body>
<div class="se-pre-con"></div>

<?php
$curPage=2;
include_once '_relativePath.php';
include_once $relPath.'_dir.php';
include_once $relPath.'_header.php'; ?>


<!--- THE CONTENT HERE ----->

<div class='container'>
<h2><span class='glyphicon glyphicon-wrench'> </span>  Demo.. </h2>

    <?php  include_once $relPath.'assets_includes/_create/addDemo.php';
    ?>

</div>



<?php  include_once $relPath.'_footer.php';

// ALL JAVASCRIPT FILES
include_once $relPath.'assets_frontEnd/_javascript.php';
?>

</body>
</html>