<?php
include_once '_relativePath.php';
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Farmacia</title>

<meta property="og:type" content="website" />
  <link rel="shortcut icon" href="/assets_frontEnd/images/favicon.png"  type="image/x-icon">
  
<meta property="og:title" content="Farmacias">
<meta property="og:description" content="Farmacias.">
<meta property="og:image" content="http://www.wtechbuilders.com/og-dark">
<!--<meta property="og:image" content="http://www.wtechbuilders.com/og-dark">-->
  <meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1024">
<meta property="og:image:height" content="1024">
<meta property="og:url" content="http://farmacia.www.wtechbuilders.com">
<!--<meta name="twitter:card" content="summary_large_image">-->


<!--  Non-Essential, But Recommended -->

<meta property="og:site_name" content="Farmacia">
        <?php
    //ALL STYLE SHEET FILES
    include_once $relPath.'assets_frontEnd/_stylesheet.php';
    ?>
</head>
<body class="">
<div class="se-pre-con"></div>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php
    include_once  $relPath.'_header.php';
    ?>
    <!-- partial -->
    
         <?php
    include_once  $relPath.'nav-menu.php';
    ?>
        
        
        
        
      <!-- partial -->
    
    <?php
    if(isset($_GET['request'])){
        include_once $relPath."assets_includes/pages/lote/request.php";
    } else{
        include_once $relPath."assets_includes/pages/lote/view.php";
    }

?>
     </div>

    
    
<?php  
    //include_once $relPath.'_footer.php';

// ALL JAVASCRIPT FILES
include_once $relPath.'assets_frontEnd/_javascript.php';
?>


<script type="text/javascript">

</script

<!--MEET OUR TEAAM-->




</body>
</html>

