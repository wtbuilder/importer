<header id="intro-section" style="height: auto">
<!--    style=" background-image: url('/home-cover-nos'); background-size: contain; height: auto"-->

    <?php include_once $relPath."_header.php"; ?>
    <!-- Video -->
    <section class="view intro-section d-none d-md-block d-lg-block d-xl-block" id="home">
<!--        -->
<!--<img src="/home-cover-nos" alt="" class="img-fluid invisible"/>-->
        <div style="height: 100vh"> </div>
        <div class="mask rgba-gradient">
            <div class="container h-100 w-100 d-flex justify-content-center align-items-center">
                <div class="row pt-5 mt-3">
                    <a class="carousel-control-prev black-text" href="#carouselContent" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left"></span>
                    </a>
                    <a class="carousel-control-next black-text" href="#carouselContent" role="button" data-slide="next">
                        <span class="fa fa-chevron-right"></span>
                    </a>

                    <div class="col-md-12 white-text text-center smooth-scroll">
                        <?php include $relPath.'assets_includes/pages/index/slide-content.php'; ?>

                                            </div>

                    <ol class="carousel-indicators mt-5">
                        <li data-target="#carouselContent" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselContent" data-slide-to="1"></li>
                        <li data-target="#carouselContent" data-slide-to="2"></li>
                        <li data-target="#carouselContent" data-slide-to="3"></li>
                        <li data-target="#carouselContent" data-slide-to="4"></li>
                    </ol>

                </div>
            </div>
        </div>


    </section>

</header>

<section class=" d-md-none  d-lg-none d-xl-none" id="home">

    <img src="/home-cover" alt="" class="img-fluid"/>

</section>

<div class="mb-0 d-lg-none d-xl-none">
    <div class="wtb-blue-text text-center black-textr">
        <?php

        if(!isset($_COOKIE['NZK-ENG'])){
            ?>

            <a class="nav-link" onClick="changeLang('eng')" href='#'>Change Language</a>
            <?php
        } else{
            ?>
            <a class="nav-link" onClick="changeLang('pt')" href='#'>Mudar lingua</a>
            <?php

        }
        ?>
    </div>
</div>
<!--