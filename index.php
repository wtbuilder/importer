<?php
include_once '_relativePath.php';
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Farmacia</title>

<meta property="og:type" content="website" />
  <link rel="shortcut icon" href="/assets_frontEnd/images/favicon.png"  type="image/x-icon">
  
<meta property="og:title" content="Farmacias">
<meta property="og:description" content="Farmacias.">
<meta property="og:image" content="http://www.wtechbuilders.com/og-dark">
<!--<meta property="og:image" content="http://www.wtechbuilders.com/og-dark">-->
  <meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1024">
<meta property="og:image:height" content="1024">
<meta property="og:url" content="http://farmacia.www.wtechbuilders.com">
<!--<meta name="twitter:card" content="summary_large_image">-->


<!--  Non-Essential, But Recommended -->

<meta property="og:site_name" content="Farmacia">
        <?php
    //ALL STYLE SHEET FILES
    include_once $relPath.'assets_frontEnd/_stylesheet.php';
    ?>
</head>
<body class="">
<div class="se-pre-con"></div>

 <div id="login-form" class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-oned">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
                 <img src="/assets_FrontEnd/_img/brand/logo-dona1-07-01.jpg" class="img-fluid mb-3" alt="logo" 
                      />
              <form action="/home" method="post" class="vform-framed small-textt" data-autosubmit="no">
                <div class="vform-item  vw-40">
                  <label for="code" class="labell">Farmacia</label>
                  <div class="input-group">
                    <input type="number_format" name="code" id="code" class="form-control validate" placeholder="Codigo da farmacia" required>
<!--
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
-->
                  </div>
                </div>
                  <div class="form-group vform-item">
                  <label class="labell" for="username">Nome de Usuario</label>
                  <div class="input-group">
                    <input type="text" name="username" class="form-control validate" placeholder="nome de usuario" required>
<!--
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
-->
                  </div>
                </div>
                <div class="form-group vform-item">
                  <label class="labell" for="password">Senha</label>
                  <div class="input-group">
                    <input type="password" name="password" class="form-control validate" placeholder="*********" required>
<!--
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
-->
                  </div>
                </div>
                <div class="form-group">
                  <button class="vbtn vbtn-green btn-block">ENTRAR</button>
                </div>
                <div class="txt-center RobotoBold mt-5">
                    
                  <p class="txt-farm"><a href="javascript:;" ><span class="txt-farm RobotoBold">Esqueceu a senha?</span></a></p>
                  <p><strong>A sua farmacia ja esta registada?</strong></p>
                  <p class="txt-farm"><a href="javascript:;" ><span class="txt-farm">Crie uma conta</span> </a></p>
                
                  </div>
              </form>
            </div>
            </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>





<?php  
    //include_once $relPath.'_footer.php';

// ALL JAVASCRIPT FILES
include_once $relPath.'assets_frontEnd/_javascript.php';
?>


<script type="text/javascript">

</script

<!--MEET OUR TEAAM-->




</body>
</html>

