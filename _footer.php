
<!----------------- Footer Start -------------------->
    <footer id="footer" class="page-footer font-small black-text" >
 <!-- Footer Links -->
    <div class="py-4 container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3 ">


            <!-- Grid column All(4) -->

            <!------ Grid column I Section Services ---->
            <div class="col-12">
                <div id="contact-footer" class="text-center">
                    <h5 class=" text-uppercase mb-3 black-text">FIND US AROUND</h5>
                    <a href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                    <a href="#" class="ml-4" target="_blank"><span class="fa fa-twitter"></span></a>
                    <a href="#" class="ml-4" target="_blank"> <span class="fa fa-instagram"></span></a>
                    <a href="#" class="ml-4" target="_blank"> <span class="fa fa-youtube"></span></a>


<!--                    <a href="#" class="ml-4 ml-sm-auto" target="_blank"><span class="fa fa-soundcloud"></span></a>-->
<!--                    <a href="#" class="ml-4 ml-sm-auto" target="_blank"><span class="fa fa-spotify"></span></a>-->
                    <p class="mt-3">info@niozik.wtechbuilders.com</p>
                </div>
                <p style="font-size: 0.8rem" class=" mt-4 pull-right">POWERED BY : <a target="_blank" class=" black-text" href="http://wtechbuilders.com">WISE TECH BUILDERS</a></p>
            </div>


        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->
    <style>
        .footerbox{
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 -2px 10px 0 rgba(0,0,0,.12) !important;
        }
    </style>
    <!-- Copyright -->

    <div class=" footerbox text-center  py-3 black-text wtb-bg-black"><span class=" white-text">Copyright © 2018 Niozik - <?php wtbString('all-rights-reserved'); ?>
</span>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->


