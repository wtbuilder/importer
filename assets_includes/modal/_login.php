<!-- Modal -->
<div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content form-elegant radius-modal" style="border-radius: 5%">
            <!--Header-->
            <div class="modal-header text-center">

                <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-2 d-block" id="myModalLabel"
               ><strong>Sign in</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="row my-3 d-flex justify-content-center">
                <!--Facebook-->
                <button type="button" class="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook text-center"></i></button>
                <!--Twitter-->
                <button type="button" class="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-twitter"></i></button>
                <!--Google +-->
                <button type="button" class="btn btn-white btn-rounded z-depth-1a"><i class="fa fa-google-plus"></i></button>
            </div>

            <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> OR</p>

            <!--Body-->
            <div class="modal-body mx-4">
                <!--Body-->
                <div class="md-form mb-5">
                    <input type="email" id="Form-email1" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="Form-email1">Your email</label>
                </div>

                <div class="md-form pb-3">
                    <input type="password" id="Form-pass1" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="Form-pass1">Your password</label>

                </div>

                <div class="text-center mb-3">
                    <button type="button" class="btn btn-dark btn-block btn-rounded z-depth-1sa">Sign in</button>
                </div>



            </div>
            <!--Footer-->
            <div class="modal-footer mx-5 pt-3 mb-1">
                <p class="font-small blue-text d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1"> Password?</a></p>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->

<!--<div class="text-center">-->
<!--    <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#elegantModalForm">Launch modal Login Form</a>-->
<!--</div>-->