<!-- Button trigger modal-->
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalYT">Launch modal</button>-->

<!--Modal: modalYT-->
<div class="modal fade" id="modalYT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Body-->
            <div class="modal-body mb-0 p-0">

                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/mJJbXx90_pY" allowfullscreen></iframe>
                </div>


            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center flex-column flex-md-row">
                <span class="mr-4"><?php wtbString('spread'); ?>!</span>
                <div>
<!--                    <a type="button" class="btn-floating btn-sm btn-fb">-->
<!--                        <i class="fa fa-facebook"></i>-->
<!--                    </a>-->
<!--                    <!--Twitter-->
<!--                    <a type="button" class="btn-floating btn-sm btn-tw">-->
<!--                        <i class="fa fa-twitter"></i>-->
<!--                    </a>-->
<!--                    <!--Google +-->
<!--                    <a type="button" class="btn-floating btn-sm btn-gplus">-->
<!--                        <i class="fa fa-google-plus"></i>-->
<!--                    </a>-->
<!--                    <!--Linkedin-->
<!--                    <a type="button" class="btn-floating btn-sm btn-ins">-->
<!--                        <i class="fa fa-linkedin"></i>-->
<!--                    </a>-->
                    <!--FB-->
<!--                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore btn-floating btn-sm btn-fb">Share</a></div>-->
                    <div
                            class="fb-like"
                            data-share="true"
                            data-width="450"
                            data-show-faces="true">
                    </div>

                </div>
                <button type="button" class="btn btn-outline-dark btn-rounded btn-md ml-4 " data-dismiss="modal"><?php wtbString('close'); ?></button>


            </div>

        </div>
        <!--/.Content-->

    </div>
</div>
<!--Modal: modalYT-->