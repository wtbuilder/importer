<!--Modal: modalTools-->
<div class="modal fade" id="modalTools" tabindex="-1" role="dialog"  data-interval="9000" aria-labelledby="myModalLabel" aria-hidden="true">
    
    
    
    
    <div class="modal-dialog modal-lg cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header light-blue darken-3 white-text">
                <h1 class="title mr-auto mx-auto text-uppercase text-center"><?php wtbString('tools'); ?></h1>
                <button type="button" class="close align-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

          <!------------------ Black Diamond start ------------->
        <!------------------ Black Diamond start ------------->
        <section class="mx-auto py-4" >
          
          <hgroup><h2 id="textheader" class="text-center font-bold  mx-auto text-uppercase mr-auto"> <span ><?php wtbString('tools'); ?> </span><span class=""></span></h2></hgroup>
        
        <P class = "text-center text-uppercase "><br><?php wtbString('our-tools'); ?> </P>
        
        <hr class="black accent-2 mb-4 mt-0  mx-auto text-center" style="width: 60px;">
        
      
        </section>
       <!--Thumbnails-->

            
            <div class=" col-lg-12">
         
                <!----- Controls Start ----->
                
                <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon"  aria-hidden="true"><img src="/assets_frontEnd/_img/clients/left.png" style=" padding-bottom:30 width:30px;"></span>
        <span class="sr-only" >Previous</span>
    </a>
    <a class="carousel-control-next " href="#carousel-thumb" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"><img src="/assets_frontEnd/_img/clients/right.png" style=" padding-bottom:30 width:30px;"></span>
        <span class="sr-only">Next</span>
    </a>
    
                <!----- Controls Start End ----->  
                <div class="col-lg-10 mx-auto mr-auto py-0 pt-0 mb-2">
     
   
    
          <!--Carousel Wrapper-->
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">

                            <!--Slides-->
                            <div class="carousel-inner text-center text-md-left" role="listbox">
                                <div class="carousel-item active">
                                    <img src="/vbuilder-logo" alt="Second slide" class="img-fluid">
                                    <div class="text-center">

                                        <p class="text-center w-responsive mx-auto mb-4"><?php wtbString('vbuilder-text'); ?></p>

                                        <a href="http://vbuilder.wtechbuilders.com" class="btn-sm btn btn-outline-dark btn-rounded btn-md mt-3 " target="_blank"><?php wtbString('access'); ?></a>

                                    </div>
                                </div>

                                <div class="carousel-item">
                                    <img src="/wtbvibes-cover" alt="Second slide" class="img-fluid">
                                    <div class="text-center">

                                        <p class="text-center w-responsive mx-auto mb-4 mt-4"><?php wtbString('wtbvibes-text'); ?></p>

                                        <a href="http://vibes.wtechbuilders.com" class="btn-sm btn btn-outline-dark btn-rounded btn-md mt-3 " target="_blank"><?php wtbString('access'); ?></a>

                                    </div>
                                </div>
<!--                                <div class="carousel-item">-->
<!--                                    <img src="/assets_frontEnd/_img/vbuilder-logo.png" alt="Second slide" class="img-fluid">-->
<!--                                </div>-->
                            </div>





                            <!--/.Slides-->
  
                            <!--/.Thumbnails-->
                       
                        </div>
                        <!--/.Carousel Wrapper-->
                    </div>
                  
            </div>
                </div>
        <!--/.Content-->

    </div>
</div>