<!---->
<!--<style>-->
<!---->
<!--    @media (min-width: 768px) {-->
<!---->
<!--    /* show 3 items */-->
<!--    .carousel-inner .active,-->
<!--    .carousel-inner .active + .carousel-item,-->
<!--    .carousel-inner .active + .carousel-item + .carousel-item,-->
<!--    .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item  {-->
<!--        display: block;-->
<!--    }-->
<!---->
<!--    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),-->
<!--    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,-->
<!--    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,-->
<!--    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {-->
<!--        transition: none;-->
<!--    }-->
<!---->
<!--    .carousel-inner .carousel-item-next,-->
<!--    .carousel-inner .carousel-item-prev {-->
<!--      position: relative;-->
<!--      transform: translate3d(0, 0, 0);-->
<!--    }-->
<!---->
<!--    .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {-->
<!--        position: absolute;-->
<!--        top: 0;-->
<!--        right: -25%;-->
<!--        z-index: -1;-->
<!--        display: block;-->
<!--        visibility: visible;-->
<!--    }-->
<!---->
<!--    /* left or forward direction */-->
<!--    .active.carousel-item-left + .carousel-item-next.carousel-item-left,-->
<!--    .carousel-item-next.carousel-item-left + .carousel-item,-->
<!--    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,-->
<!--    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,-->
<!--    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {-->
<!--        position: relative;-->
<!--        transform: translate3d(-100%, 0, 0);-->
<!--        visibility: visible;-->
<!--    }-->
<!---->
<!--    /* farthest right hidden item must be abso position for animations */-->
<!--    .carousel-inner .carousel-item-prev.carousel-item-right {-->
<!--        position: absolute;-->
<!--        top: 0;-->
<!--        left: 0;-->
<!--        z-index: -1;-->
<!--        display: block;-->
<!--        visibility: visible;-->
<!--    }-->
<!---->
<!--    /* right or prev direction */-->
<!--    .active.carousel-item-right + .carousel-item-prev.carousel-item-right,-->
<!--    .carousel-item-prev.carousel-item-right + .carousel-item,-->
<!--    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,-->
<!--    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,-->
<!--    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {-->
<!--        position: relative;-->
<!--        transform: translate3d(100%, 0, 0);-->
<!--        visibility: visible;-->
<!--        display: block;-->
<!--        visibility: visible;-->
<!--    }-->
<!---->
<!--}-->
<!---->
<!---->
<!-- /* Bootstrap Lightbox using Modal */-->
<!---->
<!--#profile-grid { overflow: auto; white-space: normal; }-->
<!--#profile-grid .profile { padding-bottom: 40px; }-->
<!--#profile-grid .panel { padding: 0 }-->
<!--#profile-grid .panel-body { padding: 15px }-->
<!--#profile-grid .profile-name { font-weight: bold; }-->
<!--#profile-grid .thumbnail {margin-bottom:6px;}-->
<!--#profile-grid .panel-thumbnail { overflow: hidden; }-->
<!--#profile-grid .img-rounded { border-radius: 4px 4px 0 0;}</style>-->

<!-- Carrousel thumbs CSS -->

<section class="carousel-fkr mt-5">

    <section class="mx-auto py-4" >

        <hgroup><h2 id="textheader" class="text-center font-bold  mx-auto text-uppercase"> <span ><?php wtbString('our-clients'); ?> </span><span class=""></span></h2></hgroup>

        <P class = "text-center font-bold"><br><?php wtbString('clients'); ?></P>

        <hr class="black accent-2 mb-4 mt-0  mx-auto text-center" style="width: 60px;">

        <div class=" py-2 container">





        </div>

    </section>
    <div class="container-fluid col-8  mx-auto  ">





        <div id="carouselClients" class="carousel slide" data-ride="carousel" data-interval="9000" style="padding-top:15px;">
            <div class="carousel-inner carousel-inner3 row w-100 mx-auto " role="listbox">

                <div class="carousel-item col-md-3 active carousel-item3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail ">
                            <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/client/akp.png" alt="slide 1">


                        </div>
                    </div>
                </div>


                <div class="carousel-item  col-md-3 carousel-item3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail ">

                            <img class="img-fluid mx-auto d-block " src="/assets_frontEnd/_img/client/3dots.png" alt="slide 2">

                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3  carousel-item3 ">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail ">

                            <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/client/cp.png" alt="slide 3">


                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3    carousel-item3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail ">

                            <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/client/meduza.png" alt="slide 4">

                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3  carousel-item3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail ">

                            <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/client/perola.png" alt="slide 5">

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>


