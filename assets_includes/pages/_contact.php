<!--Section: Contact v.2-->
<section id="wtb-contact" class="section pt-5   container text-center mr-auto">

    <!--Section heading-->
    <h2 class="h3-responsive text-uppercase font-weight-bold text-center mb-5 pt-5"> <?php wtbString('contact-header'); ?> </h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-4"><?php wtbString('contact-text'); ?></p>

    
        
    <div class="row">

        <!--Grid column-->
        <div class="col-md-12 mb-md-0 mb-5">
            <form action="/home" method="POST" id="MailwtbForm"  onsubmit='validateMailwtb(); return false;'>

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class=""><?php wtbString('your-name'); ?></label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class=""><?php wtbString('your-email'); ?></label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class=""><?php wtbString('subject'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input style="border: none" type="file" id="attachment" name="attachment" class="form-control">
<!--                            <label for="subject" class="">--><?php //wtbString('attachment'); ?><!--</label>-->
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message"><?php wtbString('your-message'); ?></label>
                        </div>

                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div id="MailwtbTarget">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--Grid row-->
                <div class="text-center text-center">
                    <button class="btn btn-outline-black btn-md waves-effect"><?php wtbString('send'); ?></button>
                </div>
            </form>


            <div class="status"></div>
        </div>
        <!--Grid column-->

        
    </div>

</section>
<!--Section: Contact v.2-->

<?php

//include_once $relPath."assets_includes/ajax/mail.php";
?>


<!-- Modal: modalAbandonedCart-->
<!-- Central Modal Medium Info -->
<div class="modal fade" id="centralModalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-info" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead"><?php wtbString('sent'); ?></p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>
                    <p><?php wtbString('sent-success'); ?></p>
                </div>
            </div>

            <!--Footer-->

            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-dark btn-rounded btn-md ml-4 " data-dismiss="modal"><?php wtbString('close'); ?></button>


            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Central Modal Medium Info-->
<!-- Central Modal Medium Danger -->
<div class="modal fade" id="centralModalDanger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-danger" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead"><?php wtbString('message'); ?></p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <i class="fa fa-remove fa-4x mb-3 animated rotateIn"></i>
                    <p><?php wtbString('sent-error'); ?></p>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-red btn-rounded btn-md ml-4 " data-dismiss="modal"><?php wtbString('close'); ?></button>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal: modalAbandonedCart-->

<script  type="text/javascript">

    function validateMailwtb(){


        var name= document.getElementById('name').value;
        var email= document.getElementById('email').value;
        var subject= document.getElementById('subject').value;
        var message= document.getElementById('message').value;

        if( name=='' || email=='' || subject=='' || message==''){
            alert("<?php wtbString('data-missing'); ?>");
            $("#MailwtbTarget").html("<p class='alert alert-danger'> <?php wtbString('data-missing'); ?> <span class=' glyphicon glyphicon-remove'></span>   </p>");
        } else{
            addMailwtb();
        }
    }
    //////////////


    function showMailwtbForm(){
        $('#MailwtbForm').trigger("reset");
        $('#MailwtbForm').show();
        $('#MailwtbTarget').html('');
        $("#ajaxLoader").hide();

    }



    /////////////

    function addMailwtb(){



        var name= document.getElementById('name').value;
        var email= document.getElementById('email').value;
        var subject= document.getElementById('subject').value;
        var message= document.getElementById('message').value;

        ////////////////////////

        var xhrMailwtb= new XMLHttpRequest();
        xhrMailwtb.open('POST','/assets_includes/ajax/mail.php',true);
        xhrMailwtb.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        var dataMailwtb='name='+name+'&email='+email+'&subject='+subject+'&message='+message;

        xhrMailwtb.onreadystatechange= function () {
            if(xhrMailwtb.readyState===4 && xhrMailwtb.status===200){
                $("#ajaxLoader").hide();
                $("#MailwtbTarget").html("");

                var resultMailwtb=JSON.parse(xhrMailwtb.responseText);
                if(resultMailwtb.state==1){
                    $('#centralModalInfo').modal('show');
                    document.getElementById("MailwtbForm").reset();
                    // $('#MailwtbForm').hide();
                    // $("#MailwtbTarget").html("<p class='alert alert-success'> ADDED WITH SUCCESS <span class=' glyphicon glyphicon-ok'></span>   </p>");
                    //   window.location.href="index.php"; changing location
                    // $('#testForm').trigger("reset"); resting form data
                } if(resultMailwtb.state==0){
                    $('#centralModalDanger').modal('show');
                    // $("#MailwtbTarget").html("<p class='alert alert-warning'> ERROR ADDING <span class=' glyphicon glyphicon-remove'></span>   </p>");
                } if(resultMailwtb.state!=1 && resultMailwtb.state!=0){
                    $('#centralModalDanger').modal('show');
                    // $("#MailwtbTarget").html("<p class='alert alert-danger'> OCCURED AN ERROR DURRING THE PROCESS, PLEASE CONTACT THE ADMINS.. <span class=' glyphicon glyphicon-remove'></span>   </p>");
                }
            }
            if(xhrMailwtb.readyState<4){
                $("#ajaxLoader").show();
            }
        };

        xhrMailwtb.send(dataMailwtb);
//END

    }


</script>