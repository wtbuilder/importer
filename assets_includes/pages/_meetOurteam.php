<style>@media (min-width: 768px) {

        /* show 3 items */
        .carousel-fkr .carousel-inner .active,
        .carousel-fkr .carousel-inner .active + .carousel-item,
        .carousel-fkr .carousel-inner .active + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item  {
            display: block;
        }

        .carousel-fkr  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
        .carousel-fkr .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
        .carousel-fkr .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
            transition: none;
        }

        .carousel-fkr .carousel-inner .carousel-item-next,
        .carousel-fkr .carousel-inner .carousel-item-prev {
            position: relative;
            transform: translate3d(0, 0, 0);
        }

        .carousel-fkr .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -25%;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* left or forward direction */
        .carousel-fkr .active.carousel-item-left + .carousel-item-next.carousel-item-left,
        .carousel-fkr .carousel-item-next.carousel-item-left + .carousel-item,
        .carousel-fkr .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }

        /* farthest right hidden item must be abso position for animations */
        .carousel-fkr .carousel-inner .carousel-item-prev.carousel-item-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* right or prev direction */
        .carousel-fkr .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
        .carousel-fkr .carousel-item-prev.carousel-item-right + .carousel-item,
        .carousel-fkr .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
        .carousel-fkr .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }

    }

    /* Bootstrap Lightbox using Modal */

    .carousel-fkr #profile-grid { overflow: auto; white-space: normal; }
    .carousel-fkr #profile-grid .profile { padding-bottom: 40px; }
    .carousel-fkr #profile-grid .panel { padding: 0 }
    .carousel-fkr #profile-grid .panel-body { padding: 15px }
    .carousel-fkr #profile-grid .profile-name { font-weight: bold; }
    .carousel-fkr #profile-grid .thumbnail {margin-bottom:6px;}
    .carousel-fkr #profile-grid .panel-thumbnail { overflow: hidden; }
    .carousel-fkr #profile-grid .img-rounded { border-radius: 4px 4px 0 0;}</style>




<section id="about-us" class="carousel-fkr">

    <!---------------------  Section of Info Start --------------->
    <!---------------------  Section of Info Start --------------->


    <hgroup><h2 id="textheader" class="text-center font-bold text-uppercase mb-2 pt-5"><?php wtbString('meet-our-team'); ?></h2></hgroup>

    <P class = "text-center font-bold display-6"><br><?php wtbString('our-team'); ?> </P>


    <hr class="black accent-2 mb-4 mt-0 mx-auto" style="width: 60px;">


    <!------------------------- Section of Info End  --------------------->
    <!------------------------- Section of Info End  --------------------->



    <section id="photosourteam">

        <div style="padding-top: 1px; " class=" container  text-center">


            <!------------------------------ MEET OUR TEAM START ------------------------------>
            <!------------------------------ MEET OUR TEAM START ------------------------------>


            <div class="container-fluid">


                <!--------Controls ON TOP ---------->

                <div class="controls-top text-center" style="padding-top: 15px;">

                    <a class="btn-floating" href="#carouselThumb" data-slide="prev"><i class="" style="font-size:25px;"></i><img src="/assets_frontEnd/_img/Team/left.png"></a>

                    <a class="btn-floating" href="#carouselThumb" data-slide="next"><i class="" style="font-size:25px;"></i><img src="/assets_frontEnd/_img/Team/right.png"></a>

                </div>

                <!-------/... Controls END ------>

                <div id="carouselThumb" class="carousel slide" data-ride="carousel" data-interval="9000" style="padding-top:15px;">
                    <div class="carousel-inner carousel-inner2 row w-100 mx-auto " role="listbox">

                        <div class="carousel-item col-md-3 active carousel-item2">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail ">
                                    <img class="img-fluid mx-auto d-block header" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 2">


                                </div>
                            </div>
                        </div>
                        <div class="carousel-item  col-md-3 carousel-item2">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail ">

                                    <img class="img-fluid mx-auto d-block header" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 2">



                                </div>
                            </div>
                        </div>
                        <div class="carousel-item col-md-3  carousel-item2 ">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail ">

                                    <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 3">


                                </div>
                            </div>
                        </div>
                        <div class="carousel-item col-md-3   carousel-item2">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail ">

                                    <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 4">

                                </div>
                            </div>
                        </div>
                        <div class="carousel-item col-md-3   carousel-item2">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail  ">

                                    <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 5">

                                </div>
                            </div>
                        </div>
                        <div class="carousel-item col-md-3   carousel-item2">
                            <div class="panel panel-default">
                                <div class="panel-thumbnail  ">

                                    <img class="img-fluid mx-auto d-block" src="/assets_frontEnd/_img/Team/zero.jpg" alt="slide 6">

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!------------------------------ MEET OUR TEAM  END ------------------------------>
            <!------------------------------ MEET OUR TEAM  END ------------------------------>



        </div>


    </section>






</section>



  


    