<!-- Button trigger modal-->
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalYT">Launch modal</button>-->

<!--Modal: modalYT-->
<div class="modal fade" id="buy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <!--Content-->
        <div class="modal-content bg-grey radius-modal" style="border-radius: 5%">

            <!--Body-->
            <div class="modal-body mb-0 p-0 pb-4">

                <div class="container">
                    <div class="row">
                        <div class="col-12">

                                <img src="/logo" class="w-25 d-block mx-auto mt-4 mb-3" alt="" />

<div style="font-size: 0.9rem">
    <p>LIZENZBEDINGUNGEN FÜR MAGIX PRODUKTE (E</p>
    <p>1. VERTRAGSGEGENSTAND: <br/>
        MAGIX gewährt Ihnen (Kunde) eine nicht ausschließliche Lizenz für das beiliegende MAGIX-Produkt.
        Sie erhalten das Recht, die erworbene Software und die Musik- und Video-Dateien auf einem
        Rechner (mit einer CPU oder auf einem Multiprozessor-Rechner) oder in einem Netzwerk unter der
        Bedingung zu nutzen, dass der Zugriff lediglich von einem Netzwerkrechner möglich ist. Erhalten
        verschiedene Netzwerkrechner Zugriff auf den Server ist für jeden einzelnen dieser Netzwerkrechner
        (Workstations) eine gesonderte Lizenz zu erwerben. Das geistige Eigentum oder die sonstigen
        Schutzrechte an der Software verbleiben nach wie vor bei MAGIX. Sie erkennen das geistige Eigentum
        an Software, Musik- und Video-Dateien, Sicherungskopien und Dokumentation seitens des
        Lizenzgebers an. Die Verantwortung für die vertragsgemäße Nutzung der Lizenzprogramme liegt
        beim Käufer der Programme.
        Im Hinblick auf Upgrades gilt folgendes: Upgrades sind vergünstigte Nachfolge- oder Ergänzungsversionen
        eines MAGIX-Produkts (Grundprodukt). Nur Inhaber eines Grundprodukts sind hinsicht</p>



</div>

                        </div>
                        <div class="col-1">

                        </div>
<div class="col-11">
    <form class="mt-5">
        <div class="custom-control-inline custom-checkbox mt-2">
            <input type="checkbox" class="custom-control-input" id="defaultChecked2" checked>
            <label class="custom-control-label" for="defaultChecked2">Accept agrement terms of privacy an policy</label>
        </div>
        <button type="button" class="btn btn-outline-dark btn-rounded btn-md pull-right mr-4" data-dismiss="modal"><span class="black-text">
                                     Next
                                </span> </button>
    </form>
</div>




                    </div>
                </div>


            </div>

            <!--Footer-->


        </div>
        <!--/.Content-->

    </div>
</div>
<!--Modal: modalYT-->