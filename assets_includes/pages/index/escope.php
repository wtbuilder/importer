<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php
    include_once  $relPath.'_header.php';
    ?>
    <!-- partial -->
    
         <?php
    include_once  $relPath.'nav-menu.php';
    ?>
        
        
        
        
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
       
            <div class="row">
                
                <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
<!--                      <h4 class="card-title">Default form</h4>-->
                      <p class="card-description">
                        Procure pelo medicamento que deseja <span class="h3"><span class="fa fa-search"></span></span>
                      </p>
                      <form class="forms-sample" action="/lote">
                        <div class="form-group vform-item border-bottom">
                          <label for="nomeMedicamento">Nome</label>
                          <input type="text" class="form-control" id="nomeMedicamento" placeholder="Nome do medicamento">
                        </div>
                        <div class="form-group">
                          <label for="quantidadeMedicamento">Quantidade</label>
                          <input type="number" class="form-control border-bottom" id="quantidadeMedicamento" placeholder="Quantidade do medicamento">
                        </div>
                           <button type="submit" class="btn btn-success mr-2">Submeter</button>
                     
<!--                        <button class="btn btn-light">Cancel</button>-->
                      </form>
                          
                    </div>
                  </div>
                  </div>
              </div>
            </div>
                
                <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
              
                <div class="col-12 stretch-card">
                  <div class="card ">
                    <div class="card-body txt-center align-content-vertical">
                        
                        <div class="">
                       
                            <div class="align-content-horizontal">
                             <h1 class="h1 txt-blue-darker">
                            <span class="fa fa-map-marker"></span>
                            </h1>
                              <h2 class="d mt-4">Importadora Lda,</h4>
                      <p class="card-title mt-2">
                        Nr 32434 Av. 1609, Maputo Mozambique
                      </p></div>
                            
                        </div>
                    
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
                
            </div>
            
            
          <div id="products-list" class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h3 class="card-titlesd mb-4">Medicamentos disponiveis</h3>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>
                           SERIE
                          </th>
                          <th>
                           PRODUTO
                          </th>
                          <th>
                            QUANTIDADE DISPONIVEL
                          </th>
                          <th>
                            OPERACOES
                          </th>
<!--
                          <th>
                            Sales
                          </th>
                          <th>
                            Deadline
                          </th>
-->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="font-weight-medium">
                            IMPF-31
                          </td>
                          <td>
                           Metroha
                          </td>
                          <td class="txt-centerd">
                              20 Caixas
                          </td>
                            <td> <a href="/lote" class="txt-black">
                                    <span class=" buy-btn"><span class="fa fa-eye"></span></span> </a>
                             
                                <a href="/lote">
                                <button class="ml-3 vbtn vbtn-black vbtn-circle style vbtn-small">
                            <span class="fa fa-shopping-cart"></span>
                                </button></a> 
                                
                            </td>
                        </tr>
                          
                          
                          
                        <tr>
                          <td class="font-weight-medium">
                            EDSA-54
                          </td>
                          <td>
                        Multi-Vitamin
                          </td>
                          <td class="txt-centerd">
                              34 Caixas
                          </td>
                            <td> <a href="/lote" class="txt-black">
                                    <span class=" buy-btn"><span class="fa fa-eye"></span></span> </a>
                             
                                <a href="/lote">
                                <button class="ml-3 vbtn vbtn-black vbtn-circle style vbtn-small">
                            <span class="fa fa-shopping-cart"></span>
                                </button></a> 
                                
                            </td>
                        </tr>
                          
                          
                          
                        <tr>
                          <td class="font-weight-medium">
                            IMPF-43
                          </td>
                          <td>
                           Parastamol
                          </td>
                          <td class="txt-centerd">
                              10 Caixas
                          </td>
                            <td> <a href="/lote" class="txt-black">
                                    <span class=" buy-btn"><span class="fa fa-eye"></span></span> </a>
                             
                                <a href="/lote">
                                <button class="ml-3 vbtn vbtn-black vbtn-circle style vbtn-small">
                            <span class="fa fa-shopping-cart"></span>
                                </button></a> 
                                
                            </td>
                        </tr>
                          
                          
                          
                          
                        <tr>
                          <td class="font-weight-medium">
                            CCD-43
                          </td>
                          <td>
                            Codaflu
                          </td>
                          <td class="txt-centerd">
                              2 Caixas
                          </td>
                           <td> <a href="/lote" class="txt-black">
                                    <span class=" buy-btn"><span class="fa fa-eye"></span></span> </a>
                             
                                <a href="/lote">
                                <button class="ml-3 vbtn vbtn-black vbtn-circle style vbtn-small">
                            <span class="fa fa-shopping-cart"></span>
                                </button></a> 
                                
                            </td>
                        </tr>
                     
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
         </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.wtechbuilders.com" target="_blank">Importadora Lda,</a>. Todo direitos reservados. Por   <a href="http://www.wtechbuilders.com" target="_blank">WISE TECH</a></span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Garantido e bem cuidado
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->