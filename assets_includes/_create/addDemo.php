<section id="form" class="container my-3">
    <form action="/" method="POST">

        <fieldset class="form-group row">
            <legend>Pet Questionnaire</legend>

            <div class="form-group row">
                <label class="col-sm-2 col-control-label text-sm-right" for="ownername">Owner</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="ownername" placeholder="Your Name">
                </div>
            </div><!-- form-group row -->

            <div class="form-group row">
                <label class=" col-sm-2 col-control-label" for="owneremail">Address</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="owneremail" placeholder="Address">
                </div>
            </div><!-- form-group row -->


            <div class="form-group row">
                <div class="col-sm-offset-2 col-sm-6">
                    <label class=" sr-only" for="ownercity">City</label>
                    <input class="form-control" type="text" id="ownercity" placeholder="City">
                </div><!-- form-group row -->

                <div class="col-sm-4">
                    <label class=" sr-only" for="ownerzip">Zip</label>
                    <input class="form-control" type="text" id="ownerzip" placeholder="Zip">
                </div><!-- form-group row -->
            </div><!-- form-group row -->

            <div class="form-group row">
                <div class="col-sm-2 col-control-label">
                    <label for="pettype">Pet type</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control input-lg" id="pettype">
                        <option>Choose</option>
                        <option value="cat">Dog</option>
                        <option value="cat">Cat</option>
                        <option value="bird">Other</option>
                    </select>
                </div>
            </div><!-- form-group row -->

            <div class="form-group row form-group row-lg">
                <div class="col-sm-2 col-control-label">
                    <label for="reasonforvisit">Symptoms</label>
                </div>
                <div class="col-sm-10">
                    <textarea class="form-control" id="reasonforvisit" rows="3"></textarea>
                </div>
            </div><!-- form-group row -->

            <div class="form-group row">
                <div class="col-sm-4">
                    <label>Has your pet been spayed or neutered?</label>
                </div>

                <div class="col-sm-8">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="spayneut" value="yes" checked> Yes
                        </label>
                    </div>

                    <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="spayneut" value="no"> No
                    </label>
                    </div>
                </div><!-- column -->
            </div><!-- form-group row -->

            <div class="form-group row">
                <div class="col-sm-12">
                    <label>Has the pet had any of the following in the past 30 days</label>
                </div>

                <div class="col-sm-8">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                        <input type="checkbox"> Abdominal pain
                    </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                        <input type="checkbox"> Lack of appetite
                    </label>
                    </div>

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                        <input type="checkbox"> Weakness
                    </label>
                    </div>
                </div><!-- form-group row -->

        </fieldset><!-- form-group row -->

        <div class="form-group row">
            <div class="col-sm-offset-2">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div><!-- offset col -->
        </div><!-- form-group row -->

    </form>
    </div>

</section>