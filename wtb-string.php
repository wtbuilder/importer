<?php
/**
 * Created by PhpStorm.
 * User: vway
 * Date: 7/30/2018
 * Time: 10:45 PM
 */

function wtbString($str){
    $str=strtolower($str);
    switch ($str) {



        case "explore-more":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Explore More"
                :"Explorar Mais"; echo $stringLang;
            break;
        case "share-this":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Share this page"
                :"Compartilhe esta página"; echo $stringLang;
            break;
        case "jivo":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"okuB7hWFxr"
                :"DBAeR5t9l9"; echo $stringLang;
            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
        case "fb-lang":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"en_US"
                :"pt_PT"; echo $stringLang;
            break;
        case "about-us":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"About us"
                :"Sobre Nós"; echo $stringLang;
            break;
        case "home":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"home"
                :"Início"; echo $stringLang;
            break;
        case "tools":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Tools"
                :"Ferramentas"; echo $stringLang;
            break;
        case "contact":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Contact"
                :"Contacto"; echo $stringLang;
            break;
        case "portfolio":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Portfolio"
                :"Portfólio"; echo $stringLang;
            break;


        case "close":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"close"
                :"Fechar"; echo $stringLang;
            break;
        case "cancel":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Cancel"
                :"Cancelar"; echo $stringLang;
            break;
        case "tap-to-view":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Tap to view"
                :"Clique para ver"; echo $stringLang;
            break;

        case "spread":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Spread the word"
                :"Espalhe a palavra"; echo $stringLang;
            break;

        case "our-team":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Our Team"
                :"Nossa Equipa"; echo $stringLang;
            break;

        case "meet-our-team":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Meet Our Team"
                :"Conheça A Nossa Equipa"; echo $stringLang;
            break;
        case "contact-header":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"We'd  love to hear about your project"
                :"Gostariamos de ouvir sobre o seu projecto"; echo $stringLang;
            break;
        case "contact-text":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
        matter of hours to help you."
                :"Tem alguma questão? Por favor não hesite em contactar-nos directamente. Nossa equipa responderá dentro de horas para mais assistência."; echo $stringLang;
            break;
        case "your-name":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Your name"
                :"Seu nome"; echo $stringLang;
            break;
        case "your-email":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Your email"
                :"Seu email"; echo $stringLang;
            break;
        case "your-message":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Your message"
                :"Sua mensagem"; echo $stringLang;
            break;
        case "subject":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Subject"
                :"Assunto"; echo $stringLang;
            break;

        case "send":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Send"
                :"Enviar"; echo $stringLang;
            break;
        case "sent":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Sent"
                :"Enviado"; echo $stringLang;
            break;
        case "Enviado":
            $stringLang= isset($_COOKIE['NZK-ENG'])?""
                :""; echo $stringLang;
            break;
        case "sent-success":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Sent with success"
                :"Enviado com sucesso."; echo $stringLang;
            break;
        case "sent-error":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Failed sending"
                :"Falhou ao enviar, preencha todos campos devidamente e tente novamente."; echo $stringLang;
            break;
        case "all-rights-reserved":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"All Rights Reserved"
                :"Todos Direitos Reservados"; echo $stringLang;
            break;

        case "message":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Message"
                :"Mensagem"; echo $stringLang;
            break;
        case "data-missing":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Please fill all the data"
                :"Por favor, preencha todos os campos"; echo $stringLang;
            break;
        case "attachment":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Attachment"
                :"Anexo"; echo $stringLang;
            break;
        case "access":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Access"
                :"Acessar"; echo $stringLang;
            break;


        case "beats":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Beats"
                :"Faixas"; echo $stringLang;
            break;
        case "cart":
            $stringLang= isset($_COOKIE['NZK-ENG'])?"Cart"
                :"Carrinho"; echo $stringLang;
            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;
//        case "":
//            $stringLang= isset($_COOKIE['NZK-ENG'])?""
//                :""; echo $stringLang;
//            break;

        default:
            echo "ERR STR";
            break;
    }

}